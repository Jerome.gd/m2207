package tp3;

public class Cowboy extends Humain {
	//Attributs
	private int popularite;
	private String caracteristique;

	//Constructeurs
	Cowboy(String nom){
		super("Simon");
		boisson="whiskey";
		popularite=0;
		caracteristique="vaillant";

	}

	//Accesseurs


	//M�thodes
	void tire(Brigand brigand) {
		System.out.println("le "+ caracteristique+" " + nom +" tire sur " + brigand.quelEstTonNom()+". PAN !");
		parler("Prend �a, voyou");
		
	}
	void libere(Dame dame) {
		popularite=10;
		System.out.println(dame.quelEstTonNom()+" est libre");
		dame.estLiberee();
	}

}
