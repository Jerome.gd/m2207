package tp3;

public class Brigand extends Humain {
	//Attributs
	private boolean libre;
	private String look;
	private int nbdedames, recompense;

	//Constructeurs
	Brigand (String nom){
		super("John");
		boisson="cognac";
		look="mechant";
		libre=false;
		recompense=100;
		nbdedames=0;
	}

	//Accesseurs


	//M�thodes
	int getRecompense() {
		return recompense;
	}
	String quelEstTonNom() {
		return nom + " le "+ look;
	}
	void sePresenter() {
		super.sePresenter();
		parler("J'ai l'air M�chant et j'ai enlev� "+nbdedames+" dames");
		parler("Ma t�te est mise � prix "+recompense +" !!");
	}
	void enleve(Dame dame) {
		
		parler("Ah ah ! " +dame.quelEstTonNom()+", tu es ma prisonni�re !");
	}

}
