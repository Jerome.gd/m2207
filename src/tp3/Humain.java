package tp3;

public class Humain {
	//Attributs
	protected String nom;
	protected String boisson;

	//Constructeurs
	public Humain (String bob) {
		nom=bob;
		boisson="lait";
	}

	//Accesseurs


	//M�thodes
	String quelEstTonNom() {
		return nom;
	}
	String quelleEstTaBoisson() {
		return boisson;
	}
	void parler(String texte) {
		System.out.println(nom+" :"+texte);
	}
	void sePresenter() {
		parler(" Bonjour, je suis " +quelEstTonNom()+ " et ma boisson pr�f�r�e est le "+boisson);
	}
	void boire() {
		System.out.println(nom+" :"+" Ah! un bon verre de "+ boisson+ " ! GLOUPS !");
	}
}
