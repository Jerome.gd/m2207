package tp3;

public class Dame extends Humain {
	//Attributs
	private boolean libre;


	//Constructeurs
	public Dame (String nom) {
		super ("Daisy");
		boisson="Martini";
		libre=true;
	}

	//Accesseurs


	//M�thodes
	String quelEstTonNom() {
		return "Miss " + nom;
	}
	void priseEnOtage() {
		libre=false;
		parler("Au secours !");
	}
	void estLiberee() {
		libre=true;
		parler("Merci Cowboy");
	}
	void sePresenter() {
		super.sePresenter();
		if (libre=true){
		parler("Actuellement, je suis libre");
		}
		else 
			parler("Actuellement, je suis kidnapp�e");
		
	}

}
