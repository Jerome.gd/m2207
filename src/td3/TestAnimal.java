package td3;

public class TestAnimal {

	public static void main(String[] args) {
		//Animal
		Animal a = new Animal();
		System.out.println(a.toString());

		System.out.println(a.affiche());
		System.out.println(a.cri());
		System.out.println(a.origine());

		//Chien
		System.out.println();
		Chien rex = new Chien();
		System.out.println(rex.affiche());
		System.out.println(rex.cri());
		
		//Chat
		System.out.println();
		Chat tom = new Chat();
		System.out.println(tom.affiche());
		System.out.println(tom.miauler());
	}

}
