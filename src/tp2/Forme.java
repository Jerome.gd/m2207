package tp2;

public class Forme {
	//Attributs
	private String couleur;
	private Boolean coloriage;

	//Constructeurs
	Forme(){
		couleur="orange";
		coloriage=true;
	}

	Forme(String c, boolean r){
		couleur=c;
		coloriage=r;
	}
	//Accesseurs
	String getCouleur() {
		return couleur;
	}
	void setCouleur(String c) {
		couleur=c;
	}
	boolean isColoriage () {
		return coloriage;
	}
	void setColoriage(boolean b) {
		coloriage=b;
	}
	//M�thodes
	String seDecrire() {
		return "Une forme de couleur" + " " + couleur+ " " +"et de coloriage" + " " + coloriage;
	}
}
