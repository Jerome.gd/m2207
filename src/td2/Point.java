package td2;

public class Point {

	//Attributs
	public int x,y;
		
	//Constructeurs
	public Point(int x1, int y1) {
		x=x1;
		y=y1;
	}
	
	//Accesseurs
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x=x;
	}
	
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y=y;
	}
	
	//M�thodes
	public void deplacer(int a,int b) {
		x=x+a;
		y=y+b;
	}
}
