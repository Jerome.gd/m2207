package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class MonAppliGraphique extends JFrame {
	//Atributs
	JButton b = new JButton("Hello World !");
	JButton c = new JButton("Hello World !");
	JButton d = new JButton("Hello World !");
	JButton e = new JButton("Hello World !");
	JButton f = new JButton("Hello World !");
	JLabel monLabel = new JLabel("Je suis un label");
	JTextField monTextField = new JTextField("Je suis un TetxtField");
	

	
	
	//Constructeurs
	public MonAppliGraphique () {
		super();
		this.setTitle("Ma première application");
		this.setSize(400,200);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		System.out.println("La fenetre est créée !");
		Container panneau = getContentPane();
		panneau.add(b);
		panneau.add(c);
		panneau.add(d);
		panneau.add(e);
		panneau.add(f);
		panneau.setLayout(new FlowLayout());
		panneau.add(monLabel);
		panneau.add(monTextField);
		panneau.add(e, BorderLayout.NORTH);
		panneau.add(b, BorderLayout.SOUTH);
		panneau.setLayout(new GridLayout(3,2));
		
		this.setVisible(true);
		
	}


	//Méthodes


	//Méthode Main
	public static void main(String[] args){
		MonAppliGraphique app = new MonAppliGraphique ();
		
		
	}
	
}
