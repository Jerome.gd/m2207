package tp5;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class CompteurDeClic extends JFrame implements ActionListener {

	//Attributs
	int compteur;
	JButton b = new JButton("Click !");
	JLabel monLabel = new JLabel("Vous avez cliqué " +compteur +" fois");
	

	//Constructeurs
	public CompteurDeClic () {
		super();
		this.setTitle("CompteurDeClic");
		this.setSize(200,100);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fenetre est créée !");
		Container panneau = getContentPane();
		panneau.add(b);
		panneau.add(monLabel);
		panneau.setLayout(new FlowLayout());
		this.setVisible(true);
		b.addActionListener(this);
	}
		

	//Méthodes
	public void actionPerformed (ActionEvent e){
		System.out.println("Une action a été détectée");
		compteur= compteur +1;
		monLabel.setText("Vous avez cliqué " +compteur +" fois");
	}

	//Méthode Main
	public static void main(String[] args){
		CompteurDeClic app = new CompteurDeClic ();
		}
}
