package tp4;

public class Pokemon {
	//Attributs
	private int energie,maxEnergie, cycles, puissance;
	private String nom;


	//Constructeurs
	Pokemon(String nom){
		this.nom=nom;
		maxEnergie = 50 + (int)(Math.random() *((90-50) + 1));
		energie= 30 + (int)(Math.random() * ((maxEnergie - 30) + 1));
	}

	//Accesseurs
	String getNom() {
		return nom;	
	}

	int getEnergie() {
		return energie;
	}
	int getPuissance() {
		return puissance= 3 + (int)(Math.random() * ((10 - 3) + 1));
	}


	//M�thodes
	void sePresenter() {
		System.out.println("Je suis "+ nom + ", j'ai "+ energie +" points d'energie ("+maxEnergie+"max) et une puissance de "+ puissance);
	}
	void manger() {
		this.energie = energie+ 10 + (int)(Math.random() *((30-10) + 1));
		if (energie>maxEnergie) {
			energie = maxEnergie;

		}
	}
	void vivre() {
		this.energie = energie - (20 + (int)(Math.random()*((40-20) + 1)));
		if (energie<0) {
			energie=0;		
		}
	}
	boolean isAlive() {
		if (energie>0) {
			return true ;
		}
		else return false;
	}
	void cycles() {
		cycles=0;
		while (energie>0) {
			manger();
			vivre();
			cycles++;
		}
		System.out.println(getNom()+"a vecu " +cycles+" cycles");
	}
	void perdreEnergie(int perte) {
		this.energie=energie-perte;
	}
	void attaquer(Pokemon adversaire) {
		adversaire.perdreEnergie(puissance);

	}



}
